#include <Stepper.h>
#define rotaryA 5
#define rotaryB 4

// motor
const int stepsPerRevolution = 200;
Stepper myStepper(stepsPerRevolution, 14,13,12,15);

// button
int counter = 0;
int aState;
int aLastState;

void setup() {
  pinMode (rotaryA, INPUT);
  pinMode (rotaryB, INPUT);
  // set the speed at 60 rpm:
  myStepper.setSpeed(60);
  // initialize the serial port:
  Serial.begin(9600);
  aLastState = digitalRead(rotaryA);
}

void loop() {
  aState = digitalRead(rotaryA);
  if (aState != aLastState) {
    if (digitalRead(rotaryB) != aState) {
      counter ++;
    } else {
      counter --;
    }
    Serial.print("Position: ");
    Serial.println(counter);
  }
  aLastState = aState;
//  // step one revolution  in one direction:
//  Serial.println("clockwise");
//  myStepper.step(stepsPerRevolution);
//  delay(5000);
//
//  // step one revolution in the other direction:
//  Serial.println("counterclockwise");
//  myStepper.step(-stepsPerRevolution);
//  delay(5000);
}
